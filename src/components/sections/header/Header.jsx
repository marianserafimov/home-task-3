import React, {useContext, useState} from 'react';

import { Container } from 'reactstrap';

import FormModal from '../../common/modals/form-modal';
import MovieForm from '../../common/forms/movie-form';
import Btn from '../../common/btn';
import { MovieDetailsContext } from '@/contexts/MovieDetailsContext';
import cfg from '@/cfg';
import searchBtn from '@/assets/search-button.png';

import './style.scss';

function Header() {
  const [modal, setModal] = useState(false);
  const {movieDetails, setMovieDetails} = useContext(MovieDetailsContext);
  const { formIds, texts } = cfg;

  return (
      <header>
        <Container>
          <span className="title">
              <p>{texts.firstPartLogo}</p>
              <p>{texts.secondPartLogo}</p>
          </span>

          {Object.keys(movieDetails).length
            ? <img className="search-btn" src={searchBtn} alt="search-btn-heading" onClick={() => setMovieDetails({})}/>
            : <Btn label={texts.addMovieLabel} className="btn-secondary" onClick={() => setModal(true)}/>
          }
          

          {modal && (
            <FormModal
              onClose={() => setModal(false)}
              btn1={{
                text: texts.formModal.btn1Text,
              }}
              btn2={{
                text: texts.formModal.btn2Text,
              }}
              formId={formIds.addMovieFormId}
              heading={texts.formModal.heading}
            >
              <MovieForm
                formId={formIds.addMovieFormId}
                afterSubmit={() => setModal(false)}
                isAddMovie={true}
               />
            </FormModal>
          )}
        </Container>
      </header>
  );
}

export default Header;
