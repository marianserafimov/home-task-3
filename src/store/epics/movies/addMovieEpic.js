import { ofType } from 'redux-observable';
import { mergeMap } from 'rxjs/operators';
import { merge, of } from 'rxjs';

import { ADD_MOVIE } from '../../types';
import {
  addMoviePending,
  addMovieFulfilled,
  addMovieRejected,
} from '../../actions';
import {
  openPreloaderAction,
  closePreloaderAction,
  sendApiRequest,
  notifySuccess,
  notifyError,
} from '../../actions';

import generateError from '@/utils/generateError';

const createPayload = (payload) => ({
  method: 'post',
  url: 'http://localhost:4000/movies',
  onSuccess: [
    closePreloaderAction,
    () => notifySuccess('Movie has been added'),
    (reponse) => addMovieFulfilled(reponse)
  ],
  onError: [
    addMovieRejected,
    closePreloaderAction,
    (error) => notifyError(generateError(error)),
  ],
  data: payload,
});

const meta = {
  api: 'ajaxApi',
};

const addMovieEpic$ = (action$) =>
  action$.pipe(
    ofType(ADD_MOVIE.DEFAULT),
    mergeMap(({ payload }) =>
      merge(
        of(addMoviePending(), openPreloaderAction()),
        of(sendApiRequest(createPayload(payload), meta)),
      ),
    ),
  );

export default addMovieEpic$;
