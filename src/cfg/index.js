export default {
    toastConfig: {
        autoClose: 8000,
        draggable: false,
    },
    texts: {
        somethingWentWrong: "Somthing went long!",
        edit: "Edit",
        delete: "Delete",
        confirmationModal: {
            description: "Are you sure you want to delete this movie?",
            heading: "Delete MOVIE"
        },
        editModal: {
            heading: 'Edit movie'
        },
        formModal: {
            btn1Text: 'Reset',
            btn2Text: 'Submit',
            heading: 'Add movie'
        },
        sortBy: 'Sory By',
        movieSearchHeading: 'FIND YOUR MOViE',
        movieSearchInputPLaceholder: "What do you want to watch?",
        movieSearchBtnLabel: 'search',
        movieSearchImgAlt:"heading-hero",
        firstPartLogo: 'netflix',
        secondPartLogo: 'roulette',
        addMovieLabel: "+add movie",
        
    },
    modalTypes: {
        edit: 'edit',
        delete: 'delete'
    },
    formIds: {
        editMovieFormId: 'edit_movie_form',
        addMovieFormId: 'add_movie_form'
    },
    genresInputFieldOptions: [
        {
            label: 'Action',
            value: 'action'
        },
        {
            label: 'Adventure',
            value: 'adventure'
        },
        {
            label: 'Drama',
            value: 'drama'
        },
        {
            label: 'Music',
            value: 'music'
        },
        {
            label: 'Comedy',
            value: 'comedy'
        },
        {
            label: 'Romance',
            value: 'romance'
        },
    ],
    genresFilters: [
        //First value the default loaded
        {
            label: 'All',
            value: 'all',
        },
        {
            label: 'Adventure',
            value: 'adventure',
        },
        {
            label: 'Comedy',
            value: 'comedy',
        },
        {
            label: 'Romance',
            value: 'romance',
        },
    ],
    sortingFilters: [
        //First value the default loaded
        {
            label: 'Newest',
            value: 'newest',
        },
        {
            label: 'Oldest',
            value: 'oldest',
        },
    ]
}