const generateError = (error) => {
  let errMessage = '';
  
  if (error && error.response && error.response.status) {
    switch (error.response.status) {
      case 401:
        errMessage = 'Authorization failed';
        break;
      case 403:
        errMessage = "You don't have the permissions to execute this operation.";
        break;
      case 404:
        errMessage = 'The page you are trying to open was not found';
        break;
      case 500:
        errMessage = 'Unexpected server error. Please contact your system administrator.';
        break;
      default:
        errMessage = JSON.stringify(error.response.data);
        break;
    }

    errMessage = `Error : ${errMessage} (Code: ${error.response.status})`;
  } else {
    errMessage = 'Unknown system error. Please contact your system administrator.';
  }
  return errMessage;
};

export default generateError;
