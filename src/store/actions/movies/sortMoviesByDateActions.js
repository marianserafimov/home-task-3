import { createAction } from 'redux-actions';

import { SORT_MOVIES_BY_DATE } from '../../types';

export const sortMoviesByDate = createAction(SORT_MOVIES_BY_DATE);
