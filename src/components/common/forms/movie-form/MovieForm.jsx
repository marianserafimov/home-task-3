import React from 'react';

import { Row, Col, FormGroup } from 'reactstrap';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import { useDispatch } from 'react-redux';
import * as PropTypes from 'prop-types';

import CustomSelect from '../../inputs/custom-select/CustomSelect';
import { updateMovie } from '@/store/actions';
import { addMovie } from '@/store/actions';
import ValidationError from '../validation-error';
import cfg from '@/cfg';

import './style.scss'; 

function MovieForm({ 
  formId,
  afterSubmit,
  movieInitialValues,
  isAddMovie = false
}) {
  const dispatch = useDispatch();
  const { genresInputFieldOptions } = cfg;

  const movieFormValidationSchema = Yup.object().shape({
    title: Yup.string()
      .min(5, 'Too Short!')
      .max(100, 'Too Long!')
      .required('Required'),
    release_date: Yup.date()
      .required('Required'),
    poster_path: Yup.string()
      .required('Required'),
    vote_average: Yup.number()
      .required('Required'),
    genres: Yup.array()
      .min(1, 'Choose atleast 1 genre'),
    runtime: Yup.number()
      .required('Required'),
    overview: Yup.string()
      .min(5, 'Too Short!')
      .max(200, 'Too Long!')
      .required('Required'),
  });

  return (
    <Formik
      initialValues={movieInitialValues 
                        ? movieInitialValues 
                        : {
                            genres: [],
                            tagline: 'Here is to the fools who dream.',
                            vote_count: 6782,
                            budget: 30000000,
                            revenue: 445435700,
                          }
                      }
                      
      validationSchema={movieFormValidationSchema}
      onSubmit={(values) => {
        if(isAddMovie) {
          dispatch(addMovie({...values}))
        } else {
          dispatch(updateMovie({...values}))
        }

        afterSubmit();
      }}
      className="movie-form"
    >
      {({ errors }) => (
        <Form id={formId}>
          <Row>
            <Col xs={7}>
              <FormGroup>
                <label htmlFor='title'>Title</label>
                <Field id='title' name='title'/>
                <ValidationError error={errors.title}/>
              </FormGroup>
            </Col>
            <Col xs={5}>
              <FormGroup>
                <label htmlFor='release_date'>RELEASE DATE</label>
                <Field id='release_date' name='release_date' type='date'/>
                <ValidationError error={errors.release_date}/>
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col xs={7}>
              <FormGroup>
                <label htmlFor='poster_path'>Poster</label>
                <Field id='poster_path' name='poster_path' placeholder='https://'/>
                <ValidationError error={errors.poster_path}/>
              </FormGroup>
            </Col>
            <Col xs={5}>
              <FormGroup>
                <label htmlFor='vote_average'>rating</label>
                <Field id='vote_average' name='vote_average' type="number"/>
                <ValidationError error={errors.vote_average}/>
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col xs={7}>
              <FormGroup>
                <label htmlFor='genres'>genre</label>
                <Field
                  className='custom-select'
                  name='genres'
                  options={genresInputFieldOptions}
                  component={CustomSelect}
                  placeholder='Select genre'
                  isMulti={true}
                />
                <ValidationError error={errors.genres}/>
              </FormGroup>
            </Col>
            <Col xs={5}>
              <FormGroup>
                <label htmlFor='runtime'>runtime</label>
                <Field id='runtime' name='runtime' type="number"/>
                <ValidationError error={errors.runtime}/>
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col xs={12}>
              <FormGroup>
                <label htmlFor='overview'>Overview</label>
                <Field id='overview' name='overview' type='textarea'/>
                <ValidationError error={errors.overview}/>
              </FormGroup>
            </Col>
          </Row>
        </Form>
      )}  
    </Formik>
  );
}

MovieForm.propTypes = {
  formId: PropTypes.string.isRequired,
  afterSubmit: PropTypes.func.isRequired,
  movieInitialValues: PropTypes.object,
  isAddMovie: PropTypes.bool
};

export default MovieForm;
