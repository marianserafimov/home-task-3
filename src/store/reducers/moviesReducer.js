import { createReducer } from 'redux-create-reducer';
import { sortMoviesByDate } from '@/utils/helpers';

import {
  GET_MOVIES,
  CLEAR_MOVIES,
  DELETE_MOVIE,
  UPDATE_MOVIE,
  ADD_MOVIE,
  FILTER_MOVIES_BY_GENRE,
  SORT_MOVIES_BY_DATE
} from '../types';

import cfg from '@/cfg';

const initialState = {
  data: [],
  filteredData: [],
  total: 0,
  offset: 0,
  limit: 0,
  isLoading: false,
  moviesCount: 0
};

const moviesReducer = createReducer(initialState, {
  [GET_MOVIES.DEFAULT]: (state) => ({
    ...state,
    isLoading: true, 
  }),
  [GET_MOVIES.PENDING]: (state) => ({
    ...state,
    isLoading: true,
  }),
  [GET_MOVIES.REJECTED]: (state) => ({
    ...state,
    isLoading: false,
  }),
  [GET_MOVIES.FULFILLED]: (state, { payload }) => ({
      ...payload.data,
      data: sortMoviesByDate(payload.data.data, cfg.sortingFilters[0].value),
      moviesCount: payload.data.data.length,
  }),
  [DELETE_MOVIE.FULFILLED]: (state, { payload }) => ({
      ...state,
      data: [...state.data.filter(n => n.id !== payload.data.id)],
      moviesCount: state.moviesCount - 1
  }),
  [UPDATE_MOVIE.FULFILLED]: (state, { payload }) => {
    const movies = state.data;
    const editIndexMovie = state.data.findIndex(movie => movie.id === payload.data.id);
    movies[editIndexMovie] = payload.data;
    
    return {
      ...state,
      data: [...movies]
    }
  },
  [ADD_MOVIE.FULFILLED]: (state, { payload }) => {
    return {
      ...state,
      data: [...state.data, payload.data],
      moviesCount: state.moviesCount + 1
    }
  },
  [FILTER_MOVIES_BY_GENRE]: (state, { payload }) => {
    const filteredData = state.data.filter(movie => movie.genres.map(genre => genre.toLowerCase()).includes(payload));

    return payload === cfg.genresFilters[0].value
      ? {
          ...state,
          moviesCount: state.data.length,
          filteredData: []
        }
      : {
          ...state,
          filteredData,
          moviesCount: filteredData.length
        }
  },
  [SORT_MOVIES_BY_DATE]: (state, { payload }) => {
    const sortedMovies = sortMoviesByDate(state.data, payload);

    return {
      ...state,
      data: [...sortedMovies],
    }
  },
  [CLEAR_MOVIES]: () => initialState,
});

export default moviesReducer;
