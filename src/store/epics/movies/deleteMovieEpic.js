import { ofType } from 'redux-observable';
import { mergeMap } from 'rxjs/operators';
import { merge, of } from 'rxjs';

import { DELETE_MOVIE } from '../../types';
import {
  deleteMoviePending,
  deleteMovieRejected,
} from '../../actions';

import {
  openPreloaderAction,
  closePreloaderAction,
  sendApiRequest,
  notifyError,
  notifySuccess
} from '../../actions';

import generateError from '@/utils/generateError';

const createPayload = (payload) => ({
  method: 'delete',
  url: 'http://localhost:4000/movies',
  onSuccess: [
    closePreloaderAction,
    () => notifySuccess('Movie has been deleted')
  ],
  onError: [
    deleteMovieRejected,
    closePreloaderAction,
    (error) => notifyError(generateError(error)),
  ],
  data: payload,
});

const meta = {
  api: 'ajaxApi',
};

const deleteMovieEpic$ = (action$) =>
  action$.pipe(
    ofType(DELETE_MOVIE.DEFAULT),
    mergeMap(({ payload }) =>
      merge(
        of(deleteMoviePending(), openPreloaderAction()),
        of(sendApiRequest(createPayload(payload), meta)),
      ),
    ),
  );

export default deleteMovieEpic$;
