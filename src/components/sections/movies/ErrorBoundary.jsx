import React from 'react';

import * as PropTypes from 'prop-types';

import cfg from '@/cfg';

function ErrorBoundary({ children }) {
    const OopsText = () => {
        return <h1>{cfg.texts.somethingWentWrong}</h1>
    }

    let isEverythingOkay = true;

    return <>{isEverythingOkay ? children : <OopsText/>}</>
}

ErrorBoundary.propTypes = {
  children: PropTypes.element.isRequired,
};

export default ErrorBoundary;