import { combineReducers } from 'redux';

import MoviesReducer from '../reducers/moviesReducer';
import notificationsReducer from './notificationsReducer';
import preloaderReducer from './preloaderReducer';

const rootReducer = combineReducers({
  movies: MoviesReducer,
  preloader: preloaderReducer,
  notifications: notificationsReducer,
});

export default rootReducer;
