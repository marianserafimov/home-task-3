import React from 'react';

import { useDispatch, useSelector } from 'react-redux';
import { Container } from 'reactstrap';

import { useComponentDidMount } from '../../common/hooks/lifecycles';
import { getMovies } from '@/store/actions';
import { moviesSelector, filteredMoviesSelector } from '@/store/selectors';
import MovieItem from './movie-item';
import SubMenu from './sub-menu';

import './style.scss';

function Movies() {
  const dispatch = useDispatch();

  const movies = useSelector(moviesSelector);
  const filteredMovies = useSelector(filteredMoviesSelector);

  //TODO: on unmount trigger clearMovies Action
  useComponentDidMount(() => {
    dispatch(getMovies());
  })

  const moviesToShow = (filteredMovies && filteredMovies.length)
                        ? filteredMovies
                        : movies

  return (
    <div className="black-overlay">
      <Container>
        <SubMenu/>
        <div className="movies">
          {moviesToShow && moviesToShow.length && moviesToShow.map(movie => {
            return <MovieItem 
                      movie={movie}
                      key={`movie-${movie.id}`}
                    />
          })}
        </div>
      </Container>
    </div>
  );
}

export default Movies;
