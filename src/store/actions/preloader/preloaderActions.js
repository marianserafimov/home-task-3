import { createAction } from 'redux-actions';

import { OPEN_PRELOADER, CLOSE_PRELOADER } from '../../types';

export const openPreloaderAction = createAction(OPEN_PRELOADER);

export const closePreloaderAction = createAction(CLOSE_PRELOADER);
