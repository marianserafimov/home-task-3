import createAsyncActionType from '@/utils/createAsyncActionTypes';

export const GET_MOVIES = createAsyncActionType('GET_MOVIES');
export const DELETE_MOVIE = createAsyncActionType('DELETE_MOVIE');
export const ADD_MOVIE = createAsyncActionType('ADD_MOVIE');
export const UPDATE_MOVIE = createAsyncActionType('UPDATE_MOVIE');
export const CLEAR_MOVIES = 'CLEAR_MOVIES';
export const FILTER_MOVIES_BY_GENRE = 'FILTER_MOVIES_BY_GENRE';
export const SORT_MOVIES_BY_DATE = 'SORT_MOVIES_BY_DATE';