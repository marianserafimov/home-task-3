import React from 'react';

import { Container } from 'reactstrap';
import * as PropTypes from 'prop-types';

import { minutesToHours, modifyGenres } from '@/utils/helpers';

import './style.scss';

function MovieDetails({ movie }) {
  const { 
    title,
    poster_path,
    release_date,
    genres,
    overview,
    vote_average,
    runtime
  } = movie;

  return (
      <Container className="movie-details">
        <div className="details-left">
          <img src={poster_path} alt={`movie-details-${title}-${release_date}`}/>
        </div>
        <div className="details-right">
          <div className="title-details">
            <h1>{title}</h1>
            <span>{vote_average}</span>
          </div>

          <h5>{modifyGenres(genres)}</h5>

          <div className="date">
            <span>{release_date}</span>
            <span>{minutesToHours(runtime)}</span>
          </div>

          <p>{overview}</p>
        </div>
    </Container>
  );
}

MovieDetails.propTypes = {
  movie: PropTypes.shape({
    title: PropTypes.string.isRequired,
    poster_path: PropTypes.string.isRequired,
    genres: PropTypes.arrayOf(PropTypes.string).isRequired,
    release_date: PropTypes.string.isRequired,
    overview: PropTypes.string.isRequired,
    vote_average: PropTypes.number.isRequired,
    runtime: PropTypes.number.isRequired,
  }),
};

export default MovieDetails;
